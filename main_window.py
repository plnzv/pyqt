from PyQt5.QtWidgets import QMainWindow
from ui.design import Ui_MainWindow
import sys
from PyQt5 import uic, QtCore
from PyQt5.QtWidgets import QApplication, QWidget, QTextEdit, QPushButton, QFileDialog, QLabel, QDoubleSpinBox
from PyQt5.QtGui import QPixmap, QImage
import video.video as video
import detection.detection as detect


class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        uic.loadUi('ui/main_window.ui', self)
        self.focus_direction = self.findChild(QDoubleSpinBox, 'numberFocus')
        self.button_load = self.findChild(QPushButton, 'btnLoadVideo')
        self.button_load.clicked.connect(lambda:self.btnLoadClicked())
        self.show()
        self.detection = detect.Detection()

    def btnLoadClicked(self):
        filename, _ = QFileDialog.getOpenFileUrl(self, filter="Видео (*.mp4)")
        if filename != QtCore.QUrl(''):

            video_obj = video.Video(filename.toString()[8:], self.detection, self.focus_direction.value()) 
            self.player_window = PlayerWindow(video_obj, self)
    
    def closeEvent(self, event):
        if hasattr(self, 'player_window'):
            self.player_window.closePlayer()


class Player(QtCore.QObject):
    frameProcessed = QtCore.pyqtSignal(object)
    is_worked = True
    is_play = True

    def __init__(self, video_obj):
        super().__init__()
        self.video_obj = video_obj
    
    def stop(self):
        self.is_worked = False

    def pause(self):
        self.is_play = False
    
    def play(self):
        self.is_play = True
    
    def getPlayState(self):
        return self.is_play
    
    def run(self):
        while self.is_worked:
            if self.is_play:
                ret, frame = self.video_obj.getFrame()
                if ret:
                    self.frameProcessed.emit(frame)
                else:
                    break


class PlayerWindow(QWidget):
    def __init__(self, video_obj,  parent=None):
        super(PlayerWindow, self).__init__(parent)
        uic.loadUi('ui/video_wnd.ui', self)
        self.show()

        self.thread = QtCore.QThread()
        self.player_thread = Player(video_obj)
        self.player_thread.moveToThread(self.thread)
        self.player_thread.frameProcessed.connect(self.frameProcessed)
        self.thread.started.connect(self.player_thread.run)
        self.thread.start()

        self.button_load = self.findChild(QPushButton, 'btnBack') 
        self.button_load.clicked.connect(lambda:self.btnBackClicked())

        self.button_load = self.findChild(QPushButton, 'btnPause') 
        self.button_load.clicked.connect(lambda:self.btnPauseClicked())

        self.button_load = self.findChild(QPushButton, 'btnContinue') 
        self.button_load.clicked.connect(lambda:self.btnPlayClicked())

    def btnPlayClicked(self):
        if not self.player_thread.getPlayState():
            self.player_thread.play()

    def btnPauseClicked(self):
        if self.player_thread.getPlayState():
            self.player_thread.pause()

    def btnBackClicked(self):
        self.closePlayer()
        self.close()

    def closePlayer(self):
        # del(self.player_thread)
        self.player_thread.stop()
        self.thread.exit()
        self.thread.wait()

    @QtCore.pyqtSlot(object)
    def frameProcessed(self, frame):
        label = self.findChild(QLabel, "videoPlayer")
        height, width, channel = frame.shape
        bytesPerLine = 3 * width
        image = QImage(frame.data, width, height, bytesPerLine, QImage.Format_BGR888)
        # img = Image.fromarray(frame, 'RGB')
        label.setPixmap(QPixmap(image))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    app.exec_()